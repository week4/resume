import 'package:flutter/material.dart';

Row _bildTextTitle(String lable, String des) {
  return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.alphabetic,
      children: [
        Text(lable,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                fontFamily: 'Palette',
                color: Colors.indigo[900])),
        Text(des,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 18,
                fontFamily: 'THSarabun')),
      ]);
}

Row _bildTextFont(String lable) {
  return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.alphabetic,
      children: [
        Text(lable,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 14,
              fontFamily: 'Bangna',
            )),
      ]);
}

Column _bildImage(String img) {
  return Column(
    mainAxisSize: MainAxisSize.min,
    children: [
      Image.asset(
        img,
        width: 50,
        height: 50,
        fit: BoxFit.contain,
      ),
    ],
  );
}

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('RESUME',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 32,
                          fontFamily: 'Vogue')),
                ),
              ])),
        ],
      ),
    );
    Widget nameSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _bildTextTitle('ชื่อ: ', 'นางสาวนภัสวรรณ'),
          _bildTextTitle('นามสกุล: ', 'จันทา'),
        ],
      ),
    );
    // Widget nameSection = Container(
    //   padding: const EdgeInsets.only(top: 32),
    //   // margin: const EdgeInsets.only(left: 150,right: 150),
    //   child: Row(
    //     children: [
    //       Expanded(
    //           child: Row(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         crossAxisAlignment: CrossAxisAlignment.baseline,
    //         textBaseline: TextBaseline.alphabetic,
    //         children: [
    //           Text('ชื่อ: ',
    //               style: TextStyle(
    //                   fontWeight: FontWeight.bold,
    //                   fontSize: 20,
    //                   fontFamily: 'Palette',
    //                   color: Colors.indigo[900])),
    //           Text('นางสาวนภัสวรรณ',
    //               style: TextStyle(
    //                   fontWeight: FontWeight.w600,
    //                   fontSize: 18,
    //                   fontFamily: 'THSarabun')),
    //         ],
    //       )),
    //       Expanded(
    //           child: Row(
    //         mainAxisAlignment: MainAxisAlignment.center,
    //         crossAxisAlignment: CrossAxisAlignment.baseline,
    //         textBaseline: TextBaseline.alphabetic,
    //         children: [
    //           Text('นามสกุล: ',
    //               style: TextStyle(
    //                   fontWeight: FontWeight.bold,
    //                   fontSize: 20,
    //                   fontFamily: 'Palette',
    //                   color: Colors.indigo[900])),
    //           Text('จันทา',
    //               style: TextStyle(
    //                   fontWeight: FontWeight.w600,
    //                   fontSize: 18,
    //                   fontFamily: 'THSarabun')),
    //         ],
    //       )),
    //     ],
    //   ),
    // );
    Widget nameSection2 = Container(
      // margin: const EdgeInsets.only(left: 150,right: 150),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _bildTextTitle('ชื่อเล่น: ', 'ทาม'),
              _bildTextTitle('เพศ: ', 'หญิง'),
              _bildTextTitle('อายุ: ', '22ปี'),
            ],
          )),
          // Expanded(
          //     child: Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   crossAxisAlignment: CrossAxisAlignment.baseline,
          //   textBaseline: TextBaseline.alphabetic,
          //   children: [
          //     Text('เพศ: ',
          //         style: TextStyle(
          //             fontWeight: FontWeight.bold,
          //             fontSize: 20,
          //             fontFamily: 'Palette',
          //             color: Colors.indigo[900])),
          //     Text('หญิง',
          //         style: TextStyle(
          //             fontWeight: FontWeight.w600,
          //             fontSize: 18,
          //             fontFamily: 'THSarabun')),
          //   ],
          // )),
          // Expanded(
          //     child: Row(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   crossAxisAlignment: CrossAxisAlignment.baseline,
          //   textBaseline: TextBaseline.alphabetic,
          //   children: [
          //     Text('อายุ: ',
          //         style: TextStyle(
          //             fontWeight: FontWeight.bold,
          //             fontSize: 20,
          //             fontFamily: 'Palette',
          //             color: Colors.indigo[900])),
          //     Text('22 ปี',
          //         style: TextStyle(
          //             fontWeight: FontWeight.w600,
          //             fontSize: 18,
          //             fontFamily: 'THSarabun')),
          //   ],
          // ))
        ],
      ),
    );
    Widget titleCompSkill = Container(
      padding: const EdgeInsets.only(top: 25, left: 25),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/coding.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' Computer Skill',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Palette',
                      color: Colors.indigo[900]))
            ],
          )),
        ],
      ),
    );
    Widget compSkill = Container(
      padding: const EdgeInsets.only(top: 5),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              _bildImage('images/c.png'),
              _bildImage('images/dart.png'),
              _bildImage('images/vue.png'),
              _bildImage('images/java.png'),
              _bildImage('images/html.png'),
              _bildImage('images/python.png'),
            ],
          )),
        ],
      ),
    );
    Widget compSkill2 = Container(
      padding: const EdgeInsets.only(top: 5),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              _bildImage('images/js.png'),
              _bildImage('images/jq.png'),
              _bildImage('images/git.png'),
              _bildImage('images/scratch.png'),
            ],
          )),
        ],
      ),
    );
    Widget titlePortfolio = Container(
      padding: const EdgeInsets.only(top: 25, left: 25),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/folder.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' ประวัติส่วนตัว',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Palette',
                      color: Colors.indigo[900]))
            ],
          )),
        ],
      ),
    );
    Widget titleEducation = Container(
      padding: const EdgeInsets.only(top: 25, left: 25, bottom: 5),
      child: Row(
        children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.alphabetic,
            children: [
              Image.asset(
                'images/book.png',
                width: 25,
                height: 25,
                fit: BoxFit.contain,
              ),
              Text(' การศึกษา',
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      fontFamily: 'Palette',
                      color: Colors.indigo[900]))
            ],
          )),
        ],
      ),
    );

    return MaterialApp(
        title: 'RESUME',
        theme: new ThemeData(scaffoldBackgroundColor: const Color.fromRGBO(189, 224, 254, 0.6)),
        home: Scaffold(
          appBar: AppBar(
            backgroundColor: Color.fromRGBO(254, 189, 224, 0.6),
            title: const Text("Napatsawan's Resume",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                  color: Colors.indigo,
                  fontFamily: 'Palette',
                )),
          ),
          body: ListView(
            children: [
              titleSection,
              Image.asset(
                'images/profile.jpg',
                width: 240,
                height: 272,
                fit: BoxFit.contain,
              ),
              nameSection,
              nameSection2,
              titleCompSkill,
              compSkill,
              compSkill2,
              titlePortfolio,
              Container(
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _bildTextTitle('วันเกิด: ', '28 กันยายน 1998'),
                  ],
                ))
              ])),
              Container(
                  // padding: const EdgeInsets.only(left: 50),
                  child: Row(children: [
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        _bildTextTitle('เชื้อชาติ: ', 'ไทย'),
                        _bildTextTitle('สัญชาติ: ', 'ไทย'),
                        _bildTextTitle('ศาสนา: ', 'พุทธ'),
                      ],
                    ))
                  ])),
              Container(
                  child: Row(children: [
                Expanded(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    _bildTextTitle('อีเมล: ', 'time.npsw@gmail.com'),
                  ],
                ))
              ])),
              titleEducation,
              Container(
                padding: const EdgeInsets.only(left: 50, bottom: 5),
                margin: EdgeInsets.all(10),
                child: Table(
                  children: [
                    TableRow(children: [
                      Column(children: [
                        _bildTextFont("ระดับการศึกษา"),
                        _bildTextFont('อนุบาล'),
                        _bildTextFont('ประถม'),
                        _bildTextFont('มัธยมต้น'),
                        _bildTextFont('มัธยมปลาย'),
                        _bildTextFont('อุดมศึกษา'),
                      ]),
                      Column(children: [
                        _bildTextFont('รายละเอียด'),
                        _bildTextFont('โรงเรียนบำรุงวิทยา'),
                        _bildTextFont('โรงเรียนอนุบุญเกื้อวิทยา'),
                        _bildTextFont('โรงเรียนบ้านบึง'),
                        _bildTextFont('โรงเรียนบ้านบึง'),
                        _bildTextFont('มหาวิทยาลัยบูรพา'),
                      ]),
                    ]),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
